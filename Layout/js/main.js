var timer;
window.onload = function(){
    back_gen();
	timer = setInterval(function(){
        back_gen();
	}, 1000);
};

function back_gen(){
    var numline = new Array(100);

    var h = document.getElementsByTagName('body')[0].clientHeight - 10;
    var w = document.getElementsByTagName('body')[0].clientWidth - 10;

    var col = Math.trunc(h/49.6) * Math.trunc(w/24.74);

    for(var i = 0; i < col; i++)
        numline[i] = Math.floor(Math.random() * Math.floor(10));
    

    document.getElementsByClassName('bg')[0].innerHTML = "<div class='back'>" + numline.join("</div><div class='back'>") +"</div>";
}

function stop(){
    if(timer != null){
        clearInterval(timer);
        timer = null;
        alert("Animation Stopped");
    }else{
        timer = setInterval(function(){
            back_gen();
        }, 1000);
        alert("Animation start!");
    }
        
}